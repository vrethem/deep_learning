import numpy as np

a = np.random.randn(5)
print("\n---------Start-------------")
print("Array: \t", a)        # wierd rank 1 array
print("Shape:\t", a.shape)
print("Transp:\t", a.T)
print("A*A_t: \t", np.dot(a, a.T))

print("\nWhat I actually want: 5x1")
b = np.random.rand(5, 1)    # Column vector
print("Array", b.shape, " :\n", b)
print("Transp", b.T.shape, " :\n",  b.T, "\n")  # row vector
print("B*B_t", np.dot(b, b.T).shape, " :\n", np.dot(b, b.T))

a1 = np.random.randn(4, 3)  # a.shape = (4, 3)
b1 = np.random.randn(3, 2)  # b.shape = (3, 2)
c1 = np.dot(a1, b1)  # m1xn1, m2xn2 => If and ONLY if n1 == n2
c2 = a1*a1           # ONLY if m1xn1 ==  m2xn2
d1 = np.array([[1, 2, 3]])
print("\n Matrix multiplication c1 ", c1.shape, " :\n", c1)
print("Element-wise multiplication c2 ", c2.shape, " :\n", c2)
print(d1.shape, d1 + 3)


##
##  np.dot(A, B) = Matrix-multiplication
##  A * B        = Elementwise multiplication
##
##
