import numpy as np
import random as rnd

w, h = 10, 5
lab = np.zeros((h, w))
#lab = [[0 for i in range(10)] for j in range(10)]
rnd.seed()
#print(lab[9, 9])
#print(lab[11, 9])
idx = 0
rnd_i = rnd.randint(1, h*w) - 1
for i in range(0, h*w):
    insert = rnd.randint(1, h*w) - 1
    #print("Iteration: ", idx, " | with rnd = ", "[", int(insert / 10), "]", "[", insert % 10, "]")
    #print(lab[int(insert / 10)][int(insert % 10)])

    while lab[int(int(insert / w))][int(insert % w)] != 0:
        insert = rnd.randint(1, h*w) - 1
        print("Try to insert ", insert)
    print("\t[", int(int(insert / w)), "]", "[", insert % w, "]", " -> ", i)
    lab[int(np.floor(insert / w))][ int(np.floor(insert % w))] = i
    idx += 1

#print(lab)
#print("-----\n", np.unique(lab, return_index=False, return_inverse=False, return_counts=False), "\n------\n")
for z in range(0, h*w):
    if z not in lab:
        print("Found duplicate key in array: ", "[", int(int(z / w) % w), "]", "[", z % w, "]")

sum1, sum2 = 0, 0
for x in range(0, h*w):
    sum1 += x
    sum2 += lab[int(int(x / w) % w)][int(x % w)]

print( "sum1 = ", sum1, " | sum2 = ", sum2)

